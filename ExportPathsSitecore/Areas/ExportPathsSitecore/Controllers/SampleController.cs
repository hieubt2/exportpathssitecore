﻿using Sitecore.Data.Items;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ExportPathsSitecore.Areas.ExportPathsSitecore.Controllers
{
    public class SampleController : Controller
    {
        public ActionResult Index()
        {
            Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

            Item home = master.GetItem("/sitecore/content/LyzonCorporate/Home");
            var csv = new StringBuilder();
            var lstItems = home.Axes.GetDescendants().ToList();
            foreach (var item in lstItems)
            {
                if (item.Visualization.Layout != null)
                {
                    var newLine = string.Format("{0}", item.Paths.FullPath);
                    csv.AppendLine(newLine);
                }
            }
            System.IO.File.WriteAllText("D://lyzoncorporate.csv", csv.ToString());

            return View("~/Areas/ExportPathsSitecore/Views/Sample/Index.cshtml");
        }

    }
}
